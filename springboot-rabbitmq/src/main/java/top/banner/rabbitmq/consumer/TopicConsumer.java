package top.banner.rabbitmq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import top.banner.rabbitmq.config.TopicConfig;

@Component
@Slf4j
public class TopicConsumer {

    @RabbitListener(queues = TopicConfig.HUAWEI_QUEUE_NAME)
    public void huawei(String message) {
        log.info("huawei <- {}", message);
    }

    @RabbitListener(queues = TopicConfig.XIAOMI_QUEUE_NAME)
    public void xiaomi(String message) {
        log.info("xiaomi <- {}", message);
    }

    @RabbitListener(queues = TopicConfig.PHONE_QUEUE_NAME)
    public void phone(String message) {
        log.info("phone <- {}", message);
    }
}
