package top.banner.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicConfig {
    public static final String TOPIC_EXCHANGE_NAME = "topic_exchange_name";
    public static final String XIAOMI_QUEUE_NAME = "xiaomi_queue_name";
    public static final String HUAWEI_QUEUE_NAME = "huawei_queue_name";
    public static final String PHONE_QUEUE_NAME = "phone_queue_name";


    @Bean
    public Queue xiaomiQueue() {
        return new Queue(XIAOMI_QUEUE_NAME, true, false, false);
    }

    @Bean
    public Queue huaweiQueue() {
        return new Queue(HUAWEI_QUEUE_NAME, true, false, false);
    }

    @Bean
    public Queue phoneQueue() {
        return new Queue(PHONE_QUEUE_NAME, true, false, false);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(TOPIC_EXCHANGE_NAME, true, false);
    }

    @Bean
    public Binding xiaomiQueueBinding(@Qualifier("xiaomiQueue") Queue queue,
                                      @Qualifier("topicExchange") Exchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                //xiaomi开头
                .with("xiaomi.#").noargs();

    }

    @Bean
    public Binding huaweiQueueBinding(@Qualifier("huaweiQueue") Queue queue,
                                      @Qualifier("topicExchange") Exchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                //huawei开头
                .with("huawei.#").noargs();

    }


    @Bean
    public Binding phoneQueueBinding(@Qualifier("phoneQueue") Queue queue,
                                     @Qualifier("topicExchange") Exchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                //包含phone
                .with("#.phone.#").noargs();

    }

}
