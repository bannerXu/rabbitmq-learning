package top.banner.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;

@Component
@Slf4j
public class MyCallBack implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnsCallback {

    @Resource
    private RabbitTemplate rabbitTemplate;

    //注入
    @PostConstruct
    public void init() {
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnsCallback(this);
    }

    /**
     * 交换机确认回调接口
     *
     * @param correlationData 保存回调消息ID及相关信息
     * @param ack             交换机瘦小消息
     * @param cause           可选原因，如果可用，则为 nack，否则为 null。
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        String id = correlationData != null ? correlationData.getId() : null;
        if (ack) {
            log.info("交换机已经收到ID为：{}的消息", id);
        } else {
            log.info("交换机还未收到ID为：{}的消息，原因是：{}", id, cause);
        }
    }


    /**
     * 可以在消息传递过程中不可达目的地时将消息返回给生产者
     * 消息不可路由时
     */
    @Override
    public void returnedMessage(ReturnedMessage returned) {
        log.error("消息：{}，被交换机：{}退回，退回原因：{}，路由Key：{}"
                , new String(returned.getMessage().getBody(), StandardCharsets.UTF_8)
                , returned.getExchange(), returned.getReplyText(), returned.getRoutingKey());
    }
}
