package top.banner.rabbitmq.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.banner.rabbitmq.config.DelayedQueueConfig;

import javax.annotation.Resource;
import java.util.Date;

@Slf4j
@RequestMapping("/ttl")
@RestController
public class SendMsgController {
    @Resource
    private RabbitTemplate rabbitTemplate;


    @GetMapping("/sendMsg/{message}")
    public void sendMsg(@PathVariable String message) {
        log.info("当前时间：{},发送一条时间给两个TTL队列：{}", new Date(), message);

        rabbitTemplate.convertAndSend("X", "XA", "消息来自ttl为10s的队列：" + message);
        rabbitTemplate.convertAndSend("X", "XB", "消息来自ttl为40s的队列：" + message);
    }


    @GetMapping("/sendExpirationMsg/{ttl}/{message}")
    public void sendMsg(@PathVariable Long ttl, @PathVariable String message) {
        log.info("当前时间：{},发送一条时长{}毫秒TTL消息给队列QC：{}", new Date(), ttl, message);
        rabbitTemplate.convertAndSend("X", "XC", "消息来自ttl：" + ttl + "ms，" + message, msg -> {
            msg.getMessageProperties().setExpiration(ttl.toString());
            return msg;
        });
    }

    @GetMapping("/sendDelayedMsg/{ttl}/{message}")
    public void sendDelayedMsg(@PathVariable Integer ttl, @PathVariable String message) {
        log.info("当前时间：{},发送一条时长{}毫秒消息给延迟队列delayed.queue：{}", new Date(), ttl, message);
        rabbitTemplate.convertAndSend(DelayedQueueConfig.DELAYED_EXCHANGE_NAME, DelayedQueueConfig.DELAYED_ROUTING_KEY, message, msg -> {
            msg.getMessageProperties().setDelay(ttl);
            return msg;
        });
    }


}
