package top.banner.six_交换机_直接路由;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import top.banner.utils.RabbitMqUtils;

import java.nio.charset.StandardCharsets;

public class ReceiveLogsDirect01 {
    public static final String EXCHANGE_NAME = "direct_logs";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitMqUtils.getChannel();
        //申明一个交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        //申明一个队列
        channel.queueDeclare("console", false, false, false, null);
        //队列绑定交换机
        channel.queueBind("console", EXCHANGE_NAME, "info");
        channel.queueBind("console", EXCHANGE_NAME, "warning");


        System.out.println("ReceiveLogsDirect01 等待接收消息...");

        DeliverCallback deliverCallback = (consumerTag, message) -> System.out.println("ReceiveLogsDirect01 " + message.getEnvelope().getRoutingKey() + "收到消息：" + new String(message.getBody(), StandardCharsets.UTF_8));

        channel.basicConsume("console", true, deliverCallback, consumerTag -> {
        });


    }
}
