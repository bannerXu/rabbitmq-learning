package top.banner.eight_死信队列;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import top.banner.utils.RabbitMqUtils;

import java.nio.charset.StandardCharsets;

public class Consumer02 {

    //死信队列名称
    public static final String DEAD_QUEUE = "dead_queue";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitMqUtils.getChannel();

        System.out.println("等待接收消息...");
        DeliverCallback deliverCallback = (consumerTag, message) -> System.out.println("Consumer02 接收消息：" + new String(message.getBody(), StandardCharsets.UTF_8));
        channel.basicConsume(DEAD_QUEUE, true, deliverCallback, consumerTag -> {
        });
    }
}
