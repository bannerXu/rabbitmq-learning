package top.banner.eight_死信队列;

import com.rabbitmq.client.Channel;
import top.banner.utils.RabbitMqUtils;

import java.nio.charset.StandardCharsets;

/**
 * 死信队列生产者
 */
public class Producer {
    //普通交换机
    public static final String NORMAL_EXCHANGE = "normal_exchange";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitMqUtils.getChannel();

//        AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().expiration("10000").build();

        //死信消息
        for (int i = 1; i < 11; i++) {
            String message = "info：" + i;
            channel.basicPublish(NORMAL_EXCHANGE, "zhansan", null, message.getBytes(StandardCharsets.UTF_8));
        }
    }
}
