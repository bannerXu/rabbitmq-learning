package top.banner.five_交换机_发布订阅;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import top.banner.utils.RabbitMqUtils;

import java.nio.charset.StandardCharsets;

/**
 * 消息接收者
 */
public class ReceiveLogs02 {

    public static final String EXCHANGE_NAME = "logs";

    public static void main(String[] args) throws Exception {
        Channel channel = RabbitMqUtils.getChannel();
        //声明一个交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        //声明一个临时队列
        String queueName = channel.queueDeclare().getQueue();

        //将队列绑定到交换机
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        System.out.println("ReceiveLogs02 等待接收消息...");
        DeliverCallback deliverCallback = (consumerTag, message) -> System.out.println("收到消息：" + new String(message.getBody(), StandardCharsets.UTF_8));

        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
        });

    }
}
