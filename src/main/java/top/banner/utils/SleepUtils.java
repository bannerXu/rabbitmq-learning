package top.banner.utils;

/**
 * @author XGL
 */
public class SleepUtils {
    public static void sleep(int second) {
        try {
            Thread.sleep(second * 1000L);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

    }
}
